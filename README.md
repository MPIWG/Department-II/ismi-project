# ISMI database project overview

![architecture schema](images/arch-ismi-3.2-700.png)

Elements of the project
* New ResearchSpace/Metaphactory frontend for data entry
    * code is in [ismi-metaphactory](https://gitlab.gwdg.de/MPIWG/Department-II/ismi-metaphactory)
    * modern bibliography is managed in Drupal (see blow) using [bibcite](https://drupal.org/project/bibcite) module
* Drupal website
    * website instance https://ismi.mpiwg-berlin.mpg.de
    * issue tracker in the repository [ismi-microsite](https://gitlab.gwdg.de/MPIWG/Department-II/ismi-microsite)
    * website code is part of [mpiwg-microsite](https://gitlab.gwdg.de/MPIWG/mpiwg-microsite)
* data model (old model and mapping to new data model)
    * [documentation for the migration process](https://gitlab.gwdg.de/MPIWG/Department-II/ismi-cidoc-model/wikis/home)
    * documentation, data and code is in the [ismi-cidoc-model](https://gitlab.gwdg.de/MPIWG/Department-II/ismi-cidoc-model) repository
    * the 3M mapping editor and X3ML data conversion tool are also in the ismi-cidoc-model repository
    * additional code is in the [openmind-tools](https://gitlab.gwdg.de/MPIWG/Department-II/openmind-tools) repository
* old OpenMind frontend for data entry
    * code and documentation is in [ismi Trac](https://it-dev.mpiwg-berlin.mpg.de/tracs/ismi) and [OpenMind Trac](https://it-dev.mpiwg-berlin.mpg.de/tracs/OpenMind3)
    * [old frontend](https://ismi-db.mpiwg-berlin.mpg.de/om4-ismi/) (password required)
* scanned image data service and hosting
    * image servers use [IIIF image standard](https://iiif.io)
    * image server code is in the [ismi-imageserver](https://github.com/robcast/ismi-imageserver) repository
    * image conversion and processing service code is in the [renamer](https://github.com/robcast/renamer) repository
    * image data and server instances are hosted in the [ComputeCanada](https://www.computecanada.ca/) cloud
